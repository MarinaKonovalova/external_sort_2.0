import heapq
import os
import sys
import tempfile
import uuid
from external_sort.reader import RecordReader


def temporary_file_name():
    return os.path.join(tempfile.gettempdir(), str(uuid.uuid1()))


class ExternalSorter:
    def __init__(self, input_file, output_file, ram_size):
        self.ram_size = ram_size * 1024
        self.input = input_file
        self.output = output_file
        self.temp_files_list = []

    def sort(self):
        size = 0
        buffer_lines = []
        with open(self.input, 'r') as reader:
            for line in reader:
                size += sys.getsizeof(line)
                if size <= self.ram_size:
                    buffer_lines.append(line)
                else:
                    temp_file = temporary_file_name()
                    with open(temp_file, 'w') as writer:
                        self.temp_files_list.append(temp_file)
                        for l in sorted(buffer_lines):
                            writer.write(l)
                        buffer_lines.clear()
                        buffer_lines.append(line)
                        size = sys.getsizeof(line)
        if buffer_lines:
            last_temp_file = temporary_file_name()
            with open(last_temp_file, 'w') as writer:
                for l in sorted(buffer_lines):
                    writer.write(l)
            self.temp_files_list.append(last_temp_file)

    def merge(self, start, end):
        list_readers = []
        if end < len(self.temp_files_list):
            for i in range(start, end+1):
                list_readers.append(RecordReader(self.temp_files_list[i]).__enter__())
            temp_file = temporary_file_name()
            with open(temp_file, 'w') as temp_output:
                for line in heapq.merge(list_readers[0], list_readers[1]):
                    temp_output.write(line)
        else:
            temp_file = temporary_file_name()
            with open(temp_file, 'w') as temp_output:
                for line in RecordReader(self.temp_files_list[start]).__enter__():
                    temp_output.write(line)
        return temp_file

    def external_sort(self):
        self.sort()
        while len(self.temp_files_list) > 1:
            temp_files = [self.merge(i, i+1) for i in range(0, len(self.temp_files_list), 2)]
            self.temp_files_list = temp_files

        with open(self.temp_files_list[0], 'r') as sorted_temp_file:
            with open(self.output, 'w') as output:
                for line in sorted_temp_file:
                    output.write(line)
