import argparse
import sys

from external_sort.external_sorter import ExternalSorter


def create_parser():
    parser = argparse.ArgumentParser('External sort')
    parser.add_argument('ram_size', type=int, help='the maximum number of kilobytes of text that the '
                                                   'utility loads into memory')
    parser.add_argument('input_file', type=str, help='path to the file whose rows should be sorted')
    parser.add_argument('output_file', type=str, help='path to the file to write the result to')
    return parser


def main():
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])
    ExternalSorter(namespace.input_file, namespace.output_file, namespace.ram_size).external_sort()


if __name__ == '__main__':
    main()
