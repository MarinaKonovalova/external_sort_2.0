# External Sort

### Description:
External command-line utility to sort a large file, which potentially will not fit in memory u a File line by line, sorted lexicographically. The sorting result is saved in a separate file.

The program receives input via command line arguments:

ram_size — the maximum number of kilobytes of text that the utility loads into memory.

input_file — path to the file whose rows should be sorted.

output_file — path to the file to write the result to.

####Example of calling the program:

    $ external_sort 4 input.txt output.txt
   
### Run tests:
    make test