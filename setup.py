from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='external_sort',
    version='1.0',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts':
            ['external_sorting = external_sort.__main__:main']
    }
)
