import sys

import pytest

from external_sort.external_sorter import ExternalSorter

from external_sort.__main__ import create_parser


def test_ram_size():
    assert ExternalSorter('test_input.txt', 'test_output.txt', 4).ram_size == 4 * 1024


@pytest.mark.parametrize(('input_file', 'output_file', 'ram_size'), [
    ('tests/test_input.txt', 'tests/test_output.txt', 4),
    ('tests/test_input.txt', 'tests/test_output.txt', 10),
    ('tests/test_input.txt', 'tests/test_output.txt', 200/1024),
    ('tests/test_small_input.txt', 'tests/test_small_output.txt', 200/1024)
])
def test_count_temp_files(input_file, output_file, ram_size):
    sorter = ExternalSorter(input_file, output_file, ram_size)
    sorter.sort()
    size = 0
    count = 0
    with open(input_file, 'r') as file:
        for line in file:
            size += sys.getsizeof(line)
            if size > ram_size * 1024:
                count += 1
                size = sys.getsizeof(line)
        if size > 0:
            count += 1

    assert len(sorter.temp_files_list) == count


def test_sort():
    ExternalSorter('tests/test_input.txt', 'tests/test_output.txt', 4).external_sort()
    is_equal = True
    count = 0
    with open('tests/true_output.txt', 'r') as true_output:
        with open('tests/test_output.txt', 'r') as output:
            for true_line in true_output:
                is_equal &= true_line == output.readline()
                if is_equal:
                    count += 1
    assert is_equal


def test_command_line():
    parser = create_parser()
    namespace = parser.parse_args(['4', 'tests/test_input.txt', 'tests/test_output.txt'])
    ext_sorter_true = ExternalSorter('tests/test_input.txt', 'tests/test_output.txt', 4)
    ext_sorter_args = ExternalSorter(namespace.input_file, namespace.output_file, namespace.ram_size)
    assert (ext_sorter_args.ram_size == ext_sorter_true.ram_size) and \
           (ext_sorter_args.input == ext_sorter_true.input) and \
           (ext_sorter_args.output == ext_sorter_true.output)
